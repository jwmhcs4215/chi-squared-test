import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
package org.apache.spark.examples.mllib
import org.apache.spark.mllib.linalg.{Matrix, Matrices, Vectors}
import org.apache.spark.mllib.stat.Statistics

val sc = new SparkContext("local", "Chi Squared test", new SparkConf())

val vec_inp = Vectors.dense(2, 4, 6 ,8, 10)
val vec_ans = Statistics.chiSqTest(vec_inp)

val matrix_inp = Matrices.dense(4, 2, Array(1, 2, 3, 4, 5, 6, 7, 8))
val matrix_ans = Statistics.chiSqTest(matrix_inp)

println("VECTOR")
println(vec_inp)
println(vec_ans)

println("MATRIX")
println(matrix_inp)
println(matrix_ans)

sc.stop()
System.exit(0)  